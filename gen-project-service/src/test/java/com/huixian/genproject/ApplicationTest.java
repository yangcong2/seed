/*
 * Copyright (c) 2018. Polegek
 */

package com.huixian.genproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class Description.
 *
 * @author : polegek
 * @date : Created in 2018/11/2 1:53
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {

    @Test
    public void main() {
    }
}