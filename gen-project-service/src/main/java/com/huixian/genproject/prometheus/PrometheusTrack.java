package com.huixian.genproject.prometheus;

/**
 * Description: 普罗米修斯  .
 *
 * @author : LuShunNeng
 * @date : Created in 2019/5/22 12:23 AM
 */
public class PrometheusTrack {

    /**
     * 打点前缀
     */
    public static final String PROMETHEUS_PREFIX = "gen-project";

}
