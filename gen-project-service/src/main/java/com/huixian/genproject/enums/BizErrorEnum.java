package com.huixian.genproject.enums;

import com.huixian.common2.model.IError;
import com.huixian.common2.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * 项目异常枚举.
 *
 * @author : frm
 * @date : Created in 2018/10/18 6:05 PM
 */
public enum BizErrorEnum implements IError {
    /**
     * 项目异常
     */
    SYSTEM_ERROR(999, "系统升级中，请稍后再试")
    ;

    private int baseCode;

    private String desc;

    BizErrorEnum(int baseCode, String desc) {
        this.baseCode = baseCode;
        this.desc = desc;
    }

    @Override
    public int getSystemNum() {
        return Integer.valueOf(PropertiesUtil.getProperty("com.huixian.system-num"));
    }

    @Override
    public int getLayerNum() {
        return 1;
    }

    @Override
    public String getBaseCode() {
        return StringUtils.leftPad(String.valueOf(this.baseCode), 3, "0");
    }

    @Override
    public int getCode() {
        return this.baseCode;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
