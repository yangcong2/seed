#!/bin/bash

# 初始化jvm日志文件(todo: 使用rotate工具,或者重定向到log_server的工具)
_JVM_LOG_DIR=/data/logs/jvm/${_UNIQUE_NAME}/$(/bin/date '+%Y%m%d_') #%H%M%S
mkdir -m 775 -p -v ${_JVM_LOG_DIR}   || exit 107 #664权限保证同一group的用户可以写

# -XX:MetaspaceSize=512m -XX:MaxMetaspaceSize=512m  //设置元空间大小（取代了永久区）
if [ "$HOSTNAME" = "javatest" ] ; then
        RAM_SET="-Xms256m -Xmx256m -Xmn64m -Xss256k -Dspring.profiles.active=dev -Dapollo.meta=http://10.31.51.92:8081 -Dapollo.cacheDir=/var/www/apollo-cache";
elif [ "$HOSTNAME" = "javafat" ] ; then
        RAM_SET="-Xms256m -Xmx256m -Xmn64m -Xss256k -Dspring.profiles.active=test -Dapollo.meta=http://10.66.180.199:8082 -Dapollo.cacheDir=/var/www/apollo-cache -javaagent:/var/www/java/lib/jacocoagent.jar=includes=com.huixian.*,output=tcpserver,port=${GEN_PROJECT_JACOCO_PORT},address=10.30.160.36";
elif [ "$HOSTNAME" = "javapre" ] ; then
        RAM_SET="-Xms256m -Xmx256m -Xmn64m -Xss256k -Dspring.profiles.active=pre -Dapollo.meta=http://10.0.1.240:8083 -Dapollo.cacheDir=/var/www/apollo-cache";
else
        RAM_SET="-Xms2000m -Xmx2000m -Xmn750m -Xss512k -Dspring.profiles.active=prd -Dapollo.meta=http://10.0.1.238:8084 -Dapollo.cacheDir=/var/www/apollo-cache";
fi
export JAVA_MY_OPTS=" -server -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -Xloggc:${_JVM_LOG_DIR}/gc.log -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${_JVM_LOG_DIR}/heap_dump.hprof ${RAM_SET} -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=68 -XX:+DisableExplicitGC "