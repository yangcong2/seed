/*
 * Copyright (c) 2018. Polegek
 */

package com.huixian.genproject.dubbo.facade.enums;

import com.huixian.common2.model.IError;
import com.huixian.common2.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * 业务异常枚举
 *
 * @author : Polegek
 * @date : Created in 2018/12/12 22:22
 */
public enum BizErrorEnum implements IError {
    /**
     * 业务异常
     */
    ;

    private int baseCode;
    private String desc;

    BizErrorEnum(int baseCode, String desc) {
        this.baseCode = baseCode;
        this.desc = desc;
    }

    @Override
    public int getSystemNum() {
        return Integer.valueOf(PropertiesUtil.getProperty("com.huixian.system-num"));
    }

    @Override
    public int getLayerNum() {
        return 1;
    }

    @Override
    public String getBaseCode() {
        return StringUtils.leftPad(String.valueOf(this.baseCode), 3, "0");
    }

    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}