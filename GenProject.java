/*
 * Copyright (c) 2018. Polegek
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * 项目初始化脚本
 *
 * @author : polegek
 * @date : Created in 2018/11/2 1:59
 */
public class GenProject {

    /**
     * 相关系统级参数
     */
    private static final String DIRECTORY_SEPARATOR = System.getProperty("file.separator");

    /**
     * 替换关键字集合：
     * 项目名称
     * 项目包名
     * 系统号
     * HTTP端口
     * Dubbo qos端口
     * Dubbo protocol端口
     */
    private static final String PROJECT_NAME = "gen-project";
    private static final String PROJECT_PACKAGE_NAME = "genproject";
    private static final String SYSTEM_NUM = "${GEN_PROJECT_SYSTEM_NUM}";
    private static final String SERVER_PORT = "${GEN_PROJECT_SERVER_PORT}";
    private static final String JACOCO_PORT = "${GEN_PROJECT_JACOCO_PORT}";
    private static final String DUBBO_QOS_PORT = "${GEN_PROJECT_DUBBO_QOS_PORT}";
    private static final String DUBBO_PROTOCOL_PORT = "${GEN_PROJECT_DUBBO_PROTOCOL_PORT}";
    private static final String GEN_PROJECT_XXL_PORT = "${GEN_PROJECT_XXL_PORT}";

    /**
     * 模板路径 / 新项目路径
     */
    private static File templeProjectPath;
    private static File newProjectPath;

    /**
     * 关键字替换Map
     */
    private static Map<String, String> replaceMap;
    private static Set<String> ignoreSet;

    /**
     * 项目初始化
     *
     * @param args .
     *
     * @throws IOException .
     */
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        // 获取新项目名称
        System.out.println("请输入新项目名称：");
        String newProjectName = sc.next();
        String newProjectPackageName = newProjectName.replace("-", "");

        // 获取新项目 AppId
        System.out.println("请输入新项目 AppId：");
        String newProjectId = sc.next();
        while (true) {
            try {
                Integer projectId = Integer.valueOf(newProjectId);
                if (newProjectId.length() != 3 || projectId < 100 || projectId > 999) {
                    System.out.println("项目Id必须为3位正整数(重新输入)：");
                    newProjectId = sc.next();
                    continue;
                }
                break;
            } catch (Exception e) {
                System.out.println("项目Id必须为数值(重新输入)：");
                newProjectId = sc.next();
            }
        }

        String newServerPort = "7" + newProjectId;
        String jacocoPort = "17" + newProjectId;
        String newDubboProtocolPort = "20" + newProjectId;
        String newDubboQosPort = "21" + newProjectId;
        String newXxlPort = "47" + newProjectId;

        System.out.println();
        System.out.println("==============================");
        System.out.println("   1. 项目参数                ");
        System.out.println("==============================");
        System.out.println("名称： " + newProjectName);
        System.out.println("包名： " + newProjectPackageName);
        System.out.println("AppId： " + newProjectId);
        System.out.println("Web端口： " + newServerPort);
        System.out.println("DubboQos端口： " + newDubboQosPort);
        System.out.println("DubboProtocol端口： " + newDubboProtocolPort);
        System.out.println("xxlProtocol端口： " + newXxlPort);
        System.out.println("jacoco端口： " + jacocoPort);

        replaceMap = new HashMap<>(10);
        replaceMap.put(PROJECT_NAME, newProjectName);
        replaceMap.put(PROJECT_PACKAGE_NAME, newProjectPackageName);
        replaceMap.put(SYSTEM_NUM, newProjectId);
        replaceMap.put(SERVER_PORT, newServerPort);
        replaceMap.put(JACOCO_PORT, jacocoPort);
        replaceMap.put(DUBBO_QOS_PORT, newDubboQosPort);
        replaceMap.put(DUBBO_PROTOCOL_PORT, newDubboProtocolPort);
        replaceMap.put(GEN_PROJECT_XXL_PORT, newXxlPort);


        System.out.println();
        System.out.println("==============================");
        System.out.println("   2. 新项目构建              ");
        System.out.println("==============================");

        // 项目初始化
        System.out.println("项目初始化中....");
        initProject(newProjectName);
        System.out.println("项目初始化完成");
        System.out.println();

        // 项目复制
        System.out.println("创建新项目中....");
        copyDir(templeProjectPath.toString(), newProjectPath.toString());
        System.out.println("创建完成");
        System.out.println();

        // 注意事项
        System.out.println("**********注意事项**********");
        System.out.println("根据项目部署位置（阿里云/蚂蚁云），在 `doc` 目录下对相关脚本进行区分");
        System.out.println("项目部署和文件拷贝请阅读 README.md 文档");
    }

    /**
     * 构建初始化
     *
     * @param newProjectName 新项目名称
     */
    private static void initProject(String newProjectName) {
        /*
         * 设置过滤文件夹/文件
         * 理论上，由于从git获取生成项目，因此除 .git 文件夹外，没有需要过滤的文件夹/文件
         */
        ignoreSet = new HashSet<>();
        ignoreSet.add(DIRECTORY_SEPARATOR + ".git");
        ignoreSet.add(DIRECTORY_SEPARATOR + ".idea");
        ignoreSet.add(DIRECTORY_SEPARATOR + "GenProject.java");
        ignoreSet.add(DIRECTORY_SEPARATOR + "GenProject.class");

        // 设置路径
        templeProjectPath = new File(System.getProperty("user.dir"));

        /*
         * 路径
         */
        File publicParentPath = templeProjectPath.getParentFile();
        newProjectPath = new File(publicParentPath.toString() + DIRECTORY_SEPARATOR + newProjectName);
        if (newProjectPath.mkdir()) {
            System.out.println("新项目路径： " + newProjectPath.toString());
        } else {
            System.err.println("该目录已存在，无法创建项目");
        }
    }

    /**
     * 复制文件夹
     *
     * @param sourcePath 源文件
     * @param newPath    新文件
     *
     * @throws IOException .
     */
    private static void copyDir(String sourcePath, String newPath) throws IOException {
        String ignoreValue = sourcePath.replace(templeProjectPath.toString(), "");
        if (ignoreSet.contains(ignoreValue)) {
            return;
        }

        File file = new File(sourcePath);
        String[] filePath = file.list();
        if (filePath == null || filePath.length == 0) {
            return;
        }

        if (!(new File(newPath)).exists()) {
            (new File(newPath)).mkdir();
        }

        for (String aFilePath : filePath) {
            String newPathFile = newPath + DIRECTORY_SEPARATOR + aFilePath;
            newPathFile = replaceValue(newPathFile);
            if ((new File(sourcePath + DIRECTORY_SEPARATOR + aFilePath)).isDirectory()) {
                copyDir(sourcePath + DIRECTORY_SEPARATOR + aFilePath,
                        newPathFile);
            }

            if (new File(sourcePath + DIRECTORY_SEPARATOR + aFilePath).isFile()) {
                copyFile(sourcePath + DIRECTORY_SEPARATOR + aFilePath,
                        newPathFile);
            }

        }
    }

    /**
     * 复制文件
     *
     * @param oldPath 源文件
     * @param newPath 新文件
     *
     * @throws IOException .
     */
    private static void copyFile(String oldPath, String newPath) throws IOException {
        String ignoreValue = oldPath.replace(templeProjectPath.toString(), "");
        if (ignoreSet.contains(ignoreValue)) {
            return;
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(oldPath),
                StandardCharsets.UTF_8));
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newPath),
                StandardCharsets.UTF_8));

        String str;
        while((str = in.readLine()) != null){
            str = replaceValue(str);
            out.write(str);
            out.newLine();
            out.flush();
        }

        in.close();
        out.close();
    }

    /**
     * 文本内容替换
     *
     * @param value  待替换内容
     *
     * @return .
     */
    private static String replaceValue(String value) {
        for (String key : replaceMap.keySet()) {
            value = value.replace(key, replaceMap.get(key));
        }
        return value;
    }
}
